#!/usr/bin/env python


import heapq
import math
import sys


dirs = {
    "E": (1, 0),
    # "N": (0, 1),
    "S": (0, -1),
    # "W": (-1, 0),
}


def gridstar(start, end, avoid):
    visited = set()
    best = {}
    pq = []
    # Total expected cost, Current cost, Previous position, Current position, direction
    heapq.heappush(pq, (abs(end[0] - start[0]) + abs(end[1] - start[1]), 0, start, start, ""))
    while len(pq) > 0:
        total, cost, (x, y), (xx, yy), dir = heapq.heappop(pq)
        if (xx, yy) not in best:
            best[xx, yy] = (x, y, dir)
        # print(total, cost, (x, y), (xx, yy), dir)
        if (xx, yy) == end:
            # print(best)
            path = [(xx, yy, dir)]
            px, py, dir = path[0]
            while True:
                if best.get((px, py)) is None:
                    break
                pxx, pyy, ddir = best[px, py]
                if (pxx, pyy) == (px, py):
                    break
                px, py, dir = pxx, pyy, ddir
                path = [(px, py, dir)] + path
            return path
        if (xx, yy) in visited:
            continue
        visited.add((xx, yy))
        for d, (dx, dy) in dirs.items():
            xxx, yyy = xx + dx, yy + dy
            if xxx < min(0, end[0]) or xxx > max(0, end[0]):
                continue
            if yyy < min(0, end[1]) or yyy > max(0, end[1]):
                continue
            if ((xx, yy), (xxx, yyy)) in avoid:
                continue
            heapq.heappush(pq, (abs(end[0] - xxx) + abs(end[1] - yyy), cost + 1, (xx, yy), (xxx, yyy), d))


if __name__ == "__main__":
    input = tuple(l.strip() for l in sys.stdin)
    T = int(input[0])
    for case in range(1, T + 1):
        N = int(input[2 * (case - 1) + 1])
        P = input[2 * (case - 1) + 2]
        # print(case, N, P)
        Ps = set()
        P_path = [(0, 0)]
        Px, Py = P_path[0]
        for d in P:
            dx, dy = dirs[d]
            Pxx, Pyy = Px + dx, Py + dy
            Ps.add(((Px, Py), (Pxx, Pyy)))
            Px, Py = Pxx, Pyy
            P_path.append((Px, Py))
        # print(P_path)
        path = gridstar((0, 0), (Px, Py), Ps)
        # print(list(p[:2] for p in path))
        path_string = "".join(p[2] for p in path[:-1])
        print("Case #{}: {}".format(case, path_string))
        # grid = [[" " for x in range(Px + 1)] for y in range(1 - Py)]
        # for (x, y), (xx, yy) in Ps:
        #     grid[-y][x] = "P"
        #     grid[-yy][xx] = "P"
        # for x, y, d in path:
        #     grid[-y][x] = "M"
        # for y in grid:
        #     print("".join(y))
