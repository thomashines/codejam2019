#!/usr/bin/env python


import math
import sys


fours = set()
nonfours = set()
def big_split(N):
    def has_four(nonfours, fours, num):
        if num in fours:
            return True
        elif num in nonfours:
            return False
        elif "4" in str(num):
            fours.add(num)
            return True
        else:
            nonfours.add(num)
        return False
    for A in range(N):
        if has_four(nonfours, fours, A):
            continue
        B = N - A
        if has_four(nonfours, fours, B):
            continue
        return A, B
    print("wat")
    return None, None


def slow_split(N):
    def has_four(nonfours, fours, num):
        return "4" in str(num)
    for A in range(N):
        if has_four(nonfours, fours, A):
            continue
        B = N - A
        if has_four(nonfours, fours, B):
            continue
        return A, B
    print("wat")
    return None, None


def half_split(N):
    A = list(str(math.floor(N/2)))
    B = list(str(math.ceil(N/2)))
    for cc in range(len(A)):
        if A[cc] == "4":
            A[cc] = "3"
            B[cc] = chr(ord(B[cc]) + 1)
    for cc in range(len(B)):
        if B[cc] == "4":
            B[cc] = "5"
            A[cc] = chr(ord(A[cc]) - 1)
    return int("".join(A)), int("".join(B))


if __name__ == "__main__":
    input = tuple(int(l.strip()) for l in sys.stdin)
    T = input[0]
    Ns = input[1:]
    for case in range(1, T + 1):
        N = Ns[case - 1]
        # A, B = big_split(N)
        # A, B = slow_split(N)
        A, B = half_split(N)
        print("Case #{}: {} {}".format(case, A, B))
