#include <algorithm>
#include <iostream>
#include <math.h>
#include <string>
#include <vector>

int main(int argc, char **argv)
{
  // std::cout << "3.cpp" << std::endl;

  int cases;
  std::cin >> cases;
  for (int casenum = 1; casenum <= cases; ++casenum)
  {
    // int max;
    double max;
    std::cin >> max;
    int length;
    std::cin >> length;
    // std::cout << max << std::endl;
    // std::cout << length << std::endl;

    // N
    // A,     B,     C,     D,     E
    //    ab,    bc,    cd,    de,

    // A, B, C, D, E <= N

    // ab = A * B
    // bc = B * C
    // cd = C * D
    // de = D * E

    // ab / bc = A / C
    // bc / cd = B / D
    // cd / de = C / E

    // B = ab / A
    // C = A * bc / ab
    // D = ab * cd / A * bc
    // E = A * bc * de / ab * cd

    // B / A = ab / A * A
    // C / A = bc / ab
    // D / A = ab * cd / A * A * bc
    // E / A = bc * de / ab * cd

    // ab / N <= A <= N
    // ab / N <= B <= N
    // bc / N <= B <= N
    // bc / N <= C <= N
    // cd / N <= C <= N
    // cd / N <= D <= N
    // de / N <= D <= N
    // de / N <= E <= N

    // A < C = ab < bc
    // B < D = bc < cd
    // C < E = cd < de

    typedef struct Character
    {
      char character;
      int index;
      int order;
      double min;
      double max;
      // int left, right;
      double left, right;
      bool operator() (Character &ca, Character &cb)
      {
        if (std::abs(cb.index - ca.index) == 2)
        {
          if (ca.index < cb.index)
          {
            return ca.right < cb.left;
          }
          else
          {
            return ca.left < cb.right;
          }
        }
        if (ca.max < cb.min) {
          return true;
        }
        if (ca.min > cb.max) {
          return false;
        }
        if (ca.min == cb.min) {
          return ca.max < cb.max;
        }
        if (ca.max == cb.max) {
          return ca.min < cb.min;
        }
        return ca.max < cb.max;
      }
    } Character;

    std::vector<Character> characters;

    Character character;
    character.character = '?';
    character.index = 0;
    character.order = -1;
    character.min = 1;
    character.max = max;
    character.left = -1;
    character.right = -1;
    characters.push_back(character);

    for (int ii = 0; ii < length; ++ii)
    {
      // int value;
      double value;
      std::cin >> value;
      // double min = static_cast<double>(value) / static_cast<double>(max);
      double min = value / max;

      // Previous
      characters[character.index].right = value;
      characters[character.index].min = std::max(characters[character.index].min, min);

      // Next
      ++character.index;
      character.min = min;
      character.max = max;
      character.left = value;
      character.right = -1;
      characters.push_back(character);
    }

    for (int pass = 0; pass < 100; ++pass)
    {
      std::sort(characters.begin(), characters.end(),
        [](Character &ca, Character &cb) { return ca.index < cb.index; });

      bool done = true;

      for (int ii = 0; ii < characters.size(); ++ii)
      {
        Character left;
        Character &character = characters[ii];
        Character right;
        if (ii >= 1)
        {
          left = characters[ii - 1];
          if (left.min == left.max)
          {
            character.min = character.left / left.min;
            character.max = character.min;
          }
          else
          {
            character.min = std::ceil(std::max(
              character.min,
              // static_cast<double>(character.left) / left.max
              character.left / left.max
            ));
            character.max = std::floor(std::min(
              character.max,
              // static_cast<double>(character.left) / left.min
              character.left / left.min
            ));
          }
        }
        if (ii <= characters.size() - 2)
        {
          right = characters[ii + 1];
          if (right.min == right.max)
          {
            character.min = character.right / right.min;
            character.max = character.min;
          }
          else
          {
            character.min = std::ceil(std::max(
              character.min,
              // static_cast<double>(character.right) / right.max
              character.right / right.max
            ));
            character.max = std::floor(std::min(
              character.max,
              // static_cast<double>(character.right) / right.min
              character.right / right.min
            ));
          }
        }
        done = done && (character.min == character.max);
        // characters[ii] = character;
      }

      std::sort(characters.begin(), characters.end(), character);

      for (int ii = 0; ii < characters.size(); ++ii)
      {
        if (ii == 0)
        {
          characters[ii].order = 0;
        }
        else if (characters[ii].min <= characters[ii - 1].max)
        {
          characters[ii].order = characters[ii - 1].order;
        }
        else
        {
          characters[ii].order = characters[ii - 1].order + 1;
        }
        characters[ii].character = characters[ii].order + 'A';
        // std::cout << characters[ii].order << ", " << characters[ii].character << ", " << characters[ii].index << ", " << characters[ii].min << ", " << characters[ii].max << ", " << characters[ii].left << ", " << characters[ii].right << std::endl;
      }

      // if (characters[characters.size() - 1].character == 'Z')
      // {
      //   std::cout << pass << std::endl;
      //   break;
      // }

      if (done)
      {
        // std::cout << pass << std::endl;
        break;
      }
    }

    std::sort(characters.begin(), characters.end(),
      [](Character &ca, Character &cb) { return ca.index < cb.index; });

    // for (int ii = 0; ii < characters.size(); ++ii)
    // {
    //   std::cout << characters[ii].order << ", " << characters[ii].character << ", " << characters[ii].index << ", " << characters[ii].min << ", " << characters[ii].max << ", " << characters[ii].left << ", " << characters[ii].right << std::endl;
    // }

    std::cout << "Case #" << casenum << ": ";
    for (int ii = 0; ii < characters.size(); ++ii)
    {
      std::cout << characters[ii].character;
    }
    std::cout << std::endl;

    // break;

  }

  return 0;
}
