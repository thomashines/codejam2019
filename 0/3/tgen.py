#!/usr/bin/env python

import itertools
import random
import sys

def isprime(pp):
    for oo in range(2, pp):
        if pp % oo == 0:
            return False
    return True

all_primes = []
def randprimes(pmin, pmax, n):
    pp = 2
    if all_primes:
        pp = all_primes[-1]
    while not all_primes or all_primes[-1] < pmax:
        pp += 1
        if isprime(pp):
            all_primes.append(pp)
    pool = tuple(filter(lambda pp: pmin <= pp < pmax, all_primes))
    if len(pool) >= n:
        return tuple(random.sample(pool, n))
    return None

if __name__ == "__main__":
    T = random.randrange(1, 101)
    # T = 1
    print(T)
    for cn in range(T):
        N = random.randrange(101, 10001)
        L = random.randrange(25, 101)
        print("{} {}".format(N, L))
        primes = randprimes(2, N, 26)
        cipher = dict(zip((chr(ord("A") + ii) for ii in itertools.count()), sorted(primes)))
        message = list(chr(ord("A") + ii) for ii in range(26))
        while len(message) < (L + 1):
            message.append(chr(random.randrange(ord("A"), ord("Z") + 1)))
        random.shuffle(message)
        print("Case #{}: {}".format(cn + 1, "".join(message)), file=sys.stderr)
        # print(cipher, file=sys.stderr)
        # print(tuple(cipher[cc] for cc in message), file=sys.stderr)
        encrypted = tuple(cipher[cc] for cc in message)
        products = []
        for ii in range(len(encrypted) - 1):
            products.append(str(encrypted[ii] * encrypted[ii + 1]))
        print(" ".join(products))
